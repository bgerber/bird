#!/bin/bash

CONFIG="config"

if [ "$EUID" -ne "0" ]
then
	echo "Please run as root"
	exit 1
else
	echo "Your run this script as root. May the force be with you"
fi

pp () {
	echo ""
	echo "-------------------------------------------------------"
	echo $(date +%F_%H-%M-%S)
	echo ""
}

pp

echo "Start Bird installation"

pp

if [ -f "$CONFIG" ]
then
	echo "Config file $CONFIG exists"
else
	echo "No config found. Config file will be created and filled with defaults during the installation"
	touch $CONFIG
fi

pp

echo "Check functions"

for file in $(ls functions/)
do
	bash -n functions/$file
	. functions/$file
done

pp

echo "Check user"

APP_USER="$(grep -i APP_USER $CONFIG |grep -v '#' |awk '{ print $2 }')"

if [ -z "$APP_USER" ]
then
	echo "App user is not set in config so the default will be used"
	APP_USER="bird"
	echo "app_user $APP_USER" >> $CONFIG
else
	echo "App user is set to $APP_USER"
fi

if getent passwd $APP_USER > /dev/null 2>&1
then
	echo "App user $APP_USER allready exists"
else
	useradd -m $APP_USER
	echo "App user $APP_USER created"
fi

echo "Add user $APP_USER to group sudo"
usermod -aG sudo $APP_USER
echo "$APP_USER ALL=(ALL:ALL) NOPASSWD: ALL" >> /etc/sudoers.d/$APP_USER

pp

echo "Check app directory"

APP_DIRECTORY="$(grep -i APP_DIRECTORY $CONFIG |grep -v '#' |awk '{ print $2 }')"

if [ -z "$APP_DIRECTORY" ]
then
	echo "App directory is not set in config so the default will be used"
	APP_DIRECTORY="/home/$APP_USER"
	echo "app_directory $APP_DIRECTORY" >> $CONFIG
else
	echo "App diretory is set to $APP_DIRECTORY"
fi

if [ -d "$APP_DIRECTORY" ]
then
	echo "App directory allready exist"
else
	echo "Create app directory"
	mkdir -p $APP_DIRECTORY
	echo "Created app directory on $APP_DIRECTORY"
fi

pp

echo "Check tmp directory"

TMP_DIRECTORY="$(grep -i TMP_DIRECTORY $CONFIG |grep -v '#' |awk '{ print $2 }')"

if [ -z "$TMP_DIRECTORY" ]
then
	echo "Tmp directory is not set in config so the default will be used"
	TMP_DIRECTORY="/tmp/bird"
	echo "tmp_directory $TMP_DIRECTORY" >> $CONFIG
else
	echo "Tmp diretory is set to $TMP_DIRECTORY"
fi

if [ -d "$TMP_DIRECTORY" ]
then
	echo "Tmp directory allready exist"
else
	echo "Create tmp directory"
	mkdir -p $TMP_DIRECTORY
	echo "Created tmp directory on $TMP_DIRECTORY"
fi

pp

echo "Install dependencies"

pp

echo "Install convert"

DEBIAN_FRONTEND=noninteractive apt-get -qq -y install imagemagick

echo "Install redis"

if [ "$(dpkg -s redis |grep "Status:" |grep "installed" |wc -l)" -eq 1 ]
then
	echo "Redis is already installed"
else
	redis_prepare
fi

S3_BUCKET="$(grep -i S3_BUCKET $CONFIG| grep -v '#' |awk '{ print $2 }')"

if [ -z "$S3_BUCKET" ]
then
	echo "Skip S3 provisioning"
	echo "Local images will be used"
else
	s3_prepare
fi

pp

echo "Create lib"
if [ -d "/var/lib/bird" ]
then
	echo "lib directory on /var/lib/bird allready exist"
else
	mkdir -p /var/lib/bird
fi

echo "Copy functions"

cp functions/* /var/lib/bird/
chown -R $APP_USER:$APP_USER /var/lib/bird
chmod -R 755 /var/lib/bird
cp $CONFIG $APP_DIRECTORY/config
chown $APP_USER:$APP_USER $APP_DIRECTORY/config
cp bird /usr/local/bin
sed  -i "1i \CONFIG=$APP_DIRECTORY/config" /usr/local/bin/bird
chown $APP_USER:$APP_USER /usr/local/bin/bird
chmod 755 /usr/local/bin/bird
